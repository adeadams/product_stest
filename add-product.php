<?php
include('config/connect_db.php');
require('./classes/form_validator.php');

$result = [];
if (isset($_REQUEST['q'])) {
  $q = $_REQUEST['q'];
  $assocArray = json_decode($q, true);


  // validate entries
  $validation = new FormValidator($assocArray);
  $result = $validation->validateForm();
  // print_r($result);

  // print_r($result['sku']);
  $sku = $result['sku'];
  $name = $result['name'];
  $price = $result['price'];
  $product_specific = $result['product_specific'];
  $product_type = $result['product_type'];

  // if result is valid --> save data to db
  $db_put = mysqli_prepare($connection, "CALL `add_product`('$sku', '$name', '$price', '$product_type', '$product_specific')");
  mysqli_stmt_execute($db_put);
  echo 'done!';
}
