var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function () {
  if (this.readyState == 4 && this.status == 200) {
    console.log(this.responseText);
    const result = this.responseText;
    console.log(result);
    const arr = JSON.parse(result);
    console.log(arr);

    const mergeObject = array => {
      const actualObject = array[0];
      const objectDescription = array[1];

      const mergedArray = [];
      actualObject.forEach(item => {
        const indexValue = objectDescription.findIndex(description => {
          return description.product_id === item.sku;
        });
        if (indexValue !== -1) {
          const newDescription = objectDescription[indexValue];
          const newMergedObject = { ...item, ...newDescription };
          mergedArray.push(newMergedObject);
        } else {
          return "An Error Occured";
        }
      });

      return mergedArray;
    };

    const real = mergeObject(arr);
    console.log(real);

    const html = real.map(e => {
      const html = `
          <div id="t" class="col-md-3">
            <div class="card">
                    <div class="card-header">
                        <div class="form-check">
                            <input class="form-check-input delete-checkbox" type="checkbox" value=${e.sku} id=""> 
                            ${e.product_type}
                        </div>
                    </div>
                    <div class="card-body">
                        <ul id="spec" class="list-group list-group-flush">
                            <li class="list-group-item">${e.sku}</li>
                            <li class="list-group-item">${e.name}</li>
                            <li class="list-group-item">${e.price}</li>
                            <li class="list-group-item">${e.product_specific}</li>
                        </ul>
                    </div> 
                  </div>
                </div>
               `;
      return html;
    });

    document.getElementById("t").innerHTML = html;
  }
};

xmlhttp.open("GET", "index.php?q=", true);

xmlhttp.send();
