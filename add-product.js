let val;
const dim = document.querySelector(".dimensions");
const weight = document.querySelector(".weight");
const size = document.querySelector(".size");

// get specifications inputs
const heightInput = document.getElementById("height");
const widthInput = document.getElementById("width");
const lengthInput = document.getElementById("length");
const weightInput = document.getElementById("weight");
const sizeInput = document.getElementById("size");

// remove required
widthInput.removeAttribute("required");
lengthInput.removeAttribute("required");
heightInput.removeAttribute("required");
sizeInput.removeAttribute("required");
weightInput.removeAttribute("required");

function handleChange(e) {
  console.log(typeof e.value, e.value);
  val = e.value;
  console.log("val", val);
  if (val !== "Book") {
    weight.classList.add("d-none");
    weightInput.removeAttribute("required");
  } else {
    weight.classList.remove("d-none");
    weightInput.setAttribute("required", "");
  }
  if (val === "Furniture") {
    dim.classList.remove("d-none");
    widthInput.setAttribute("required", "");
    lengthInput.setAttribute("required", "");
    heightInput.setAttribute("required", "");
  } else {
    dim.classList.add("d-none");
    widthInput.removeAttribute("required");
    lengthInput.removeAttribute("required");
    heightInput.removeAttribute("required");
  }
  if (val === "DVD-Disc") {
    size.classList.remove("d-none");
    sizeInput.setAttribute("required", "");
  } else {
    size.classList.add("d-none");
    sizeInput.removeAttribute("required");
  }
}

// form submit to server
var form = document.querySelector("#product_form");
form.onsubmit = async e => {
  e.preventDefault();
  let body = new FormData(form);
  const frm = Object.fromEntries(body.entries());

  // make req to php server based on form data
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      console.log(this.responseText);
      const result = JSON.stringify(this.responseText);
      console.log(result);
      if (result === '"done!"') {
        location.replace("index.html");
      }
    }
  };

  xmlhttp.open("POST", "add-product.php?q=" + JSON.stringify(frm), true);
  xmlhttp.send();
};
