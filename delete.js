let cbarr = [];
// event delegation to get checked boxes
document.getElementById("g").addEventListener("click", function (e) {
  if (e.target && e.target.matches("input.delete-checkbox")) {
    if (e.target.checked) {
      cbarr.push(e.target.value);
    } else {
      cbarr.pop(e.target.value);
    }
    // console.log(cbarr);
  }
});

document.getElementById("delete").addEventListener("click", function (e) {
  xmlhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      const del = JSON.stringify(this.responseText);
      console.log(del, typeof del);
      if (del === '"done!"') {
        location.reload();
      }
      // console.log(this.responseText);
    }
  };

  xmlhttp.open("POST", "delete.php?d=" + cbarr, true);

  xmlhttp.send();
});
