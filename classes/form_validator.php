<?php




class FormValidator
{

  private $data;
  // private $errors = [];
  private $result = [];
  private static $fields = ['sku', 'name', 'price', 'product_type', 'weight', 'length', 'height', 'width', 'size'];

  public function __construct($post_data)
  {
    $this->data = $post_data;
  }


  private function addResult($key, $val)
  {
    $this->result[$key] = $val;
  }

  public function validateForm()
  {
    foreach (self::$fields as $field) {
      if (!array_key_exists($field, $this->data)) {
        trigger_error("'$field' is not present in the data");
        return;
      }
    }
    $this->validateProduct();
    $this->validateType();
    return $this->result;
  }




  private function validateProduct()
  {
    $sku = $this->data['sku'];
    $name = $this->data['name'];
    $price = $this->data['price'];

    if ($sku || $name || $price) {
      $this->addResult('sku', $sku);
      $this->addResult('name', $name);
      $this->addResult('price', $price);
      $this->addResult('product_id', $sku);
      return $this->result;
    } else {
      return;
    }
  }



  private function validateType()
  {
    $val = $this->data;

    if ($val) {
      $this->validateFurniture();
      $this->validateBook();
      $this->validateDvd();
    } else {
      return;
    }
  }


  private function validateFurniture()
  {
    $type = $this->data['product_type'];

    $val = $this->data['product_type'] === 'Furniture';

    if ($val) {
      $width = $this->data['width'];
      $height = $this->data['height'];
      $length = $this->data['length'];
      $res = trim(sprintf("Dimensions: %sx%sx%s", $width, $height, $length));
      $this->addResult('product_specific', $res);
      $this->addResult('product_type', $type);
    } else {
      return;
    }
  }





  private function validateBook()
  {
    $type = $this->data['product_type'];

    $val = $this->data['product_type'] === 'Book';

    if ($val) {
      $weight = $this->data['weight'];
      $res = trim(sprintf("Weight: %s KG", $weight,));
      $this->addResult('product_specific', $res);
      $this->addResult('product_type', $type);
    } else {
      return;
    }
  }

  private function validateDvd()
  {
    $type = $this->data['product_type'];
    $val = $this->data['product_type'] === 'DVD-Disc';

    if ($val) {
      $size = $this->data['size'];
      $res  = trim(sprintf("Size: %s MB", $size,));
      $this->addResult('product_specific', $res);
      $this->addResult('product_type', $type);
    } else {
      return;
    }
  }
}
