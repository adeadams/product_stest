<?php
include('config/connect_db.php');

$db_fetch = mysqli_prepare($connection, "CALL getAllProducts()");
mysqli_stmt_execute($db_fetch);
$result1 = mysqli_stmt_get_result($db_fetch);
mysqli_stmt_next_result($db_fetch);
$result2 = mysqli_stmt_get_result($db_fetch);
$res1 = $result1->fetch_all(MYSQLI_ASSOC);
$res2 = $result2->fetch_all(MYSQLI_ASSOC);

echo json_encode(array($res1, $res2));
